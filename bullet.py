import pygame
from pygame.sprite import Sprite
class Bullet(Sprite):

    def __init__(self,ai_sttings,screen,ship):
        '''飞船位置创建一个子弹'''
        super(Bullet,self).__init__()
        self.screen=screen

        '''在00点设置矩形，在设置正确位置'''
        self.rect=pygame.Rect(0,0,ai_sttings.bullet_width,ai_sttings.bullet_height)
        self.rect.centerx=ship.rect.centerx
        self.rect.top=ship.rect.top

        self.y=float(self.rect.y)
        self.color=ai_sttings.bullet_color
        self.speed_factor=ai_sttings.bullet_speed_factor

    def update(self):
        '''向上移动子弹'''
        self.y-=self.speed_factor
        self.rect.y=self.y

    def draw_bullet(self):
        '''在屏幕上绘制子弹'''
        pygame.draw.rect(self.screen,self.color,self.rect)