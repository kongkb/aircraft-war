import pygame


class Ship():
    '''初始化飞船，初始位置'''

    def __init__(self, ai_settings, screen):

        self.screen = screen
        self.ai_settings = ai_settings

        # 加载图像，获得矩形
        self.image = pygame.image.load('images\\ship.jpg')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()

        # 飞船放置于屏幕底部中央
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom

        self.center = float(self.rect.centerx)

        # 移动标志
        self.moving_right = False
        self.moving_left = False

    def update(self):
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.center += self.ai_settings.ship_speed_factor
        if self.moving_left and self.rect.left > self.screen_rect.left:
            self.center -= self.ai_settings.ship_speed_factor

        self.rect.centerx = self.center

    def blitme(self):
        '''指定位置画飞船'''
        self.screen.blit(self.image, self.rect)
